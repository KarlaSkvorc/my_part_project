package com.example.karla.my_project_part;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class Main3Activity extends AppCompatActivity{
    private Button button;
    int index;
    int brojac;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);//ZA BAR GORE
        setContentView(R.layout.activity_main3);

        button = (Button) findViewById(R.id.button2);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openRules();
            }
        });
    }

    public void openRules(){
        Intent intent = new Intent(this, rules_ac.class);
        startActivity(intent);
    }

    public void generate(View view) {//GENERIRANJE RANDOM BROJEVI

        Random rand = new Random();
        int number = rand.nextInt(30);
        brojac++;

        if(brojac>=15){
            brojac=0;
            exit();
        }

        if(number>=0 && number<10){//obicne
            List<String> list;
            list = new ArrayList<String>();
            list.add("obicno");
            list.add("obicno");
            list.add("obicno");
            list.add("obicno");
            list.add("obicno");
            list.add("obicno");
            list.add("obicno");
            list.add("obicno");
            list.add("obicno");
            list.add("obicno");
            list.add("obicno");
            list.add("obicno");
            list.add("obicno");


            Collections.shuffle(list, new Random(System.nanoTime()));
            TextView words = (TextView) findViewById(R.id.words);
            words.setText(list.get(index++));

            TextView nasl = (TextView) findViewById(R.id.naziv);
            nasl.setText("REGURLAR TASK");

            ConstraintLayout lay = (ConstraintLayout) findViewById(R.id.layo3);
            lay.setBackgroundResource(R.drawable.nova_pozadina1);

        }
        if(number>=10 && number<20){//virus
            List<String> list2;
            list2 = new ArrayList<String>();
            list2.add("virus");
            list2.add("virus");
            list2.add("virus");
            list2.add("virus");
            list2.add("virus");
            list2.add("virus");
            list2.add("virus");
            list2.add("virus");
            list2.add("virus");
            list2.add("virus");
            list2.add("virus");
            list2.add("virus");
            list2.add("virus");



            Collections.shuffle(list2, new Random(System.nanoTime()));
            TextView words = (TextView) findViewById(R.id.words);
            words.setText(list2.get(index++));

            TextView nasl = (TextView) findViewById(R.id.naziv);
            nasl.setText("VIRUS");


            ConstraintLayout lay = (ConstraintLayout) findViewById(R.id.layo3);
            lay.setBackgroundResource(R.drawable.nova_pozadina3);

        }
        if(number>=20 && number<30){//game
            List<String> list3;
            list3 = new ArrayList<String>();
            list3.add("game");
            list3.add("game");
            list3.add("game");
            list3.add("game");
            list3.add("game");
            list3.add("game");
            list3.add("game");
            list3.add("game");
            list3.add("game");
            list3.add("game");
            list3.add("game");
            list3.add("game");
            list3.add("game");
            list3.add("game");



            Collections.shuffle(list3, new Random(System.nanoTime()));
            TextView words = (TextView) findViewById(R.id.words);
            words.setText(list3.get(index++));

            TextView nasl = (TextView) findViewById(R.id.naziv);
            nasl.setText("GAME");

            ConstraintLayout lay = (ConstraintLayout) findViewById(R.id.layo3);
            lay.setBackgroundResource(R.drawable.nova_pozadina2);
        }

    }


    public void exit(){
        Intent intent = new Intent(this, konec_ac.class);
        startActivity(intent);
    }
}
