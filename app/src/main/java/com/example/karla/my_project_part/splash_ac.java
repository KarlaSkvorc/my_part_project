package com.example.karla.my_project_part;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

public class splash_ac extends AppCompatActivity {
    private TextView tw;
    private ImageView iw;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);//ZA BAR GORE
        setContentView(R.layout.activity_splash_ac);
        iw = (ImageView)findViewById(R.id.logo);
        Animation moj_amin = AnimationUtils.loadAnimation(this, R.anim.moj_trans);
        iw.startAnimation(moj_amin);
        final Intent i = new Intent(this, language_ac.class);
        Thread timer  = new Thread(){
            public void run (){
                try {
                    sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                finally {
                    startActivity(i);
                    finish();
                }
            }
        };
        timer.start();
    }
}
