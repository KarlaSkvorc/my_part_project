package com.example.karla.my_project_part;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class MainActivity extends AppCompatActivity{
    private Button button;
    int index;
    int brojac=0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);//ZA BAR GORE
        setContentView(R.layout.activity_main);

        button = (Button) findViewById(R.id.button2);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openRules();
            }
        });
    }

    public void openRules(){
        Intent intent = new Intent(this, rules_ac.class);
        startActivity(intent);
    }

    public void openExits(){
        Intent intent2 = new Intent(this, konec_ac.class);
        startActivity(intent2);
    }

    public void generate(View view) {//GENERIRANJE RANDOM BROJEVI

        brojac++;
        if(brojac>=15) {
            openExits();
        }

        Random rand = new Random();
        int number = rand.nextInt(132);


        if(number>=0 && number<51){//obicne
                List<String> list;
                list = new ArrayList<String>();
                list.add("1.\t___, bježi oko sobe kao kokoš ili pij 2 put.");
                list.add("2.\tAko si ikad pokušavao upotrijebiti sve četiri boje kod pinkale s četiri boje, piješ 4 put.");
                list.add("3.\tDaj 2 guca ako nikad nisi gledao niti jedan film od „Gospodara prstenova“.");
                list.add("4.\tSvi koji su već bili na wcu od početka „Drinkija“, daju tri guca.");
                list.add("5.\t__, daješ toliko guca koliko imaš ujaka.");
                list.add("6.\t__, daješ tolio guca koliko imaš sestrični.");
                list.add("7.\t__, napravi stoj na rukama ili pij 3 put.");
                list.add("8.\t__, napravi zvijezdu ili pij 3 put.");
                list.add("9.\t__, napravi kolut naprijed ili pij 2 put.");
                list.add("10.\t__, daj 4 guca osobama višim od tebe. Ako je to nemoguće, ti pijes 2 guca.");
                list.add("11.\tOnaj koji drži mobitel mora odrediti tko će eksati svoje piće.ol");
                list.add("12.\t___, odredi jedan predmet u sobi. Tko ga prvi dotakne, daje 5 guca.");
                list.add("13.\t__odredi jednu osobu u sobi. Tko je prvi dotakne, daje 3 guca. Ta osoba daje 1 guc nekome.");
                list.add("14.\t__, izmiksaj „Đavolje piće“ ___. Ako ne želiš, piješ 5 put, ako ta osoba ne želi popiti, oboje pijete po svaki 10 put.");
                list.add("15.\t___, ako si u vezi daješ 4 guca, ako nisi daješ guca.");
                list.add("16.\tOnaj tko je lakirao nekome nokte daje 4 guca, onaj kome je netko lakirao nokte daje  2 guca.");
                list.add("17.\tCure, pijete onoliko kolika je vaša veličina grudnjaka (A=1, B=2, …).");
                list.add("18.\tDečki, pijete onoliko koliko je veličina vaših gaća (S=1, M=2, …).");
                list.add("19.\tPrvi tko se sjeti inspiracijske rečenice daje tri guca.");
                list.add("20.\t__, pleši Gangnam Style ili piješ 2 put.");
                list.add("21.\t__, pleši Robot Dance ili piješ 2 put.");
                list.add("22.\t__, pleši Macarenu ili piješ 2 put.");
                list.add("23.\tIgrači koji nisu glasali na zadnjim izborima piju 4 guca.");
                list.add("24.\t___, daješ 4 guca igračima koji su tanji od tebe. Ako to nije moguće, piješ 2 put.");
                list.add("25.\t__, daješ 4 guca igračima koji su stariji od tebe. Ako to nije moguće, piješ te 4 guce.");
                list.add("26.\t___, daješ 3 guca igračima koji su mlađi od tebe. Ako to nije moguče, piješ 5 guci.");
                list.add("27.\t___, ako ostaneš tiho 5 minuta, možeš odabrati tko će eksati svoje piće.");
                list.add("28.\tSvatko tko je imao seksi snove ovaj tjedan pije 3 puta.");
                list.add("29.\t__, biraj dal želiš dati 4 guca zadnjem igrači koji je bio na wcu ili 1 guc svakome u sobi.");
                list.add("30.\tSvi mlađi od ___, piju 3 put.");
                list.add("31.\tSvi stariji od ___, piju 3 put.");
                list.add("32.\t___, daj toliko guci koliko bijelih stvari nosis.");
                list.add("33.\tSvi koji su išli na faks s ___, piju 2 guca.");
                list.add("34.\tSvi koji su išli u srednju školu s ___, piju 3 guca.");
                list.add("35.\t__, ako si ikad riješio rubikovu kocku, daješ 4 guca. Ako nisi, piješ te guce.");
                list.add("36.\t__, napravi 5 sklekova ili pij 5 put.");
                list.add("37.\t__, napravi 10 sklekova ili pij 5 put.");
                list.add("38.\tPopij 1 guc ako si slobodan/na, 2 ako si u vezi, 3 ako imaš prijatelja/icu s povlasticama i 4 ako je komplicirano.");
                list.add("39.\t__, zatvori svoje oči. Ako možeš reći točno tko pije što, daješ 5 guca. Ako pogriješiš, piješ 2 guca.");
                list.add("40.\tSvi piju toliko puta koliko godina je prošlo od završetka njihove srednje škole.");
                list.add("41.\tAko studiraš piješ 2 put, ako radiš piješ 3 put. Ako niti jedno, piješ 5 put.");
                list.add("42.\t__,  govoriš englesku abecedu od iza prema početku. Ako pogriješiš, piješ 4 put.");
                list.add("43.\t___, ako bi rađe imala psa, daješ 2 guca. Ako mačku, daješ 1 guc.");
                list.add("44.\t __, piješ toliko guca koliko je praznih čaša na stolu.");
                list.add("45.\tSvi koji imaju vozačku dozvolu i aktivno ju koriste piju 1, koji nemaju vozačku dozvolu 3 put, a koji je imaju i ne koriste 5 put!");
                list.add("46.\tPije najsvježiji vozač za stolom!");
                list.add("47.\tTko ima najduže vozačku dozvolu, daje 5 guca.");
                list.add("48.\t__, popij 1 guc svojega pića bez ruku. Ako uspiješ, daješ 3 guca, ako ne, te guce popiješ ti.");
                list.add("49.\t___, ako si došao pješke ili biciklom ne piješ, ako si došao javnim prijevozom piješ 3 guca.");
                list.add("50.\tPokaži svima zadnjih 5 slika u svojoj galeriji. Ako odbiješ, piješ 5 puta.");
                list.add("51.\t__, daj svoj otključan mobitel ___ na jednu minutu. Ako odbiješ, eksaš svoje piće.");

            Collections.shuffle(list, new Random(System.nanoTime()));
                TextView words = (TextView) findViewById(R.id.words);
                words.setText(list.get(index++));

                TextView nasl = (TextView) findViewById(R.id.naziv);
                nasl.setText("OBIČAN ZADATAK");

                ConstraintLayout lay = (ConstraintLayout) findViewById(R.id.layo);
                lay.setBackgroundResource(R.drawable.nova_pozadina1);

        }
        if(number>=51 && number<81){//virus
                List<String> list2;
                list2 = new ArrayList<String>();
                list2.add("1.\t___, okreni leđa svima u sobi. Mi ćemo ti reći kad je vrijeme da se okreneš nazad.");
                list2.add("2.\t___, idi u kut i razmisli što si rekao. Mi ćemo ti reći kad je vrijeme da se vratiš.");
                list2.add("3.\tVrijeme je za „čopleka“! Svaki put kad bilo tko pije, uzmite imaginarnog čopleka dole s vašeg pića, a kad završite, morate ga staviti nazad gore.");
                list2.add("4.\tSvaki dečko pije duplo kad pije neka cura.");
                list2.add("5.\tSvaka cura pije duplo kad pije neki dečko.");
                list2.add("6.\tVrijeme je za „pirate“! Svaki igrač na početku I na kraju rečenice mora reći „ARR“. Tko pogriješi, pije 2 guca.");
                list2.add("7.\tVrijeme je za „Janeza“! Svaki put na početku i na kraju rečenice morate reći „ja ne“. Tko pogriješi, pije 2 guca.");
                list2.add("8.\tSvi u trapericama piju 2 guca svaki put dok netko pije.");
                list2.add("9.\tSvi u kratkim majicama piju 2 guca svaki put dok netko pije.");
                list2.add("10.\tSvi s crnom kosom piju guc svaki put dok pije neka cura.");
                list2.add("11.\t__, biraš dvije osobe koje će zamijeniti mjesta. Svaki put dok to napraviš, piješ 1 guc.");
                list2.add("12.\tVrijeme je za „slamku“! Svaki put kad bilo tko pije, uzmite imaginarnu slamku van s vašeg pića, a kad završite, morate je staviti nazad unutar. Ne zaboravite je držati cijelo vrijeme u ruci dok pijete i ne zaboravite je protresti da ne bi došlo do razlijevanja.");
                list2.add("13.\tVrijeme je za „tugića“! __, ti si tugić, svaki put dok se nasmiješ, moraš piti guc.");
                list2.add("14.\tZabranjeno diranje kose i lica! Tko pogriješi, pije 4 put za svaki put kad se dotaknuo/la.");
                list2.add("15.\tZabranjeno pušenje! Tko želi pušiti, mora piti 5 put.");
                list2.add("16.\tZabranjeno gledanje u oči! Tko se međusobno pogleda u oči, oboje pije 3 put.");
                list2.add("17.\tVrijeme je za „Jen Selter Izazov“! Svaki put dok se netko želi ustati iz mjesta, mora napraviti 10 čučnjeva. Onaj koji zaboravi, pije 2 put.");
                list2.add("18.\tVrijeme je za „Pauzu“! Svaki igrač neka uključi zvuk i internet na svojem mobitelu i neka stavi mobitel na sredinu stola. Svaki put dok netko dobije obavijest, daje 3 guca.");
                list2.add("19.\tSvaki put dok nekome nešto slučajno padne, njegovi susjedi piju 5 put.");
                list2.add("20.\tVrijeme je za „čajanku“! Svaki put dok netko pije, taj mora biti s podignutim malim prstom. Tko pogriješi, pije 3 guca.");
                list2.add("21.\tUozbiljimo se! Nitko se ne smije nasmijati. Tko se nasmije, pije 3 guca.");
                list2.add("22.\t__, ustaj i pleši! Ako prestaneš, popij guc!");
                list2.add("23.\t___, mađioničar si ti, abrakadabra! Zamijeni nečije piće kad kod želiš.");
                list2.add("24.\t___ je glavna/i. Svaki put d ok pljesneš rukama, svi piju (uključujući i tebe)!");
                list2.add("25.\tSvaki igrač mora govoriti kao da ima puna usta hrane. Tko zaboravi, pije 3 put!");
                list2.add("26.\t___, svaki put dok netko završi rečenicu moraš reći „očito…“. Ako zaboraviš, popij guc!");
                list2.add("27.\t__, postala si baka! Ne smiješ nikome pokazati svoje zube. Ako pokažeš, piješ 3 put.");
                list2.add("28.\t__, je postao/la sluga od __. Možeš ispuniti do 5 njegovih želji.");
                list2.add("29.\tNovo pravilo! Svaki put dok netko govori, mora završiti rečenicu su „__, ti si mutec!“.");
                list2.add("30.\tVrijeme je za kauboje i indijance! Dečki su kauboji i svaki put moraju početi rečenicu s „Howdy!“, a cure su indijanci i svaku rečenicu moraju završiti s lupanjem po ustima i vikanjem „oooo“. ");



            Collections.shuffle(list2, new Random(System.nanoTime()));
                TextView words = (TextView) findViewById(R.id.words);
                words.setText(list2.get(index++));

                TextView nasl = (TextView) findViewById(R.id.naziv);
                nasl.setText("VIRUS");

                ConstraintLayout lay = (ConstraintLayout) findViewById(R.id.layo);
                lay.setBackgroundResource(R.drawable.nova_pozadina3);

        }
        if(number>=81 && number<132){//game
                List<String> list3;
                list3 = new ArrayList<String>();
                list3.add("1.\tGovorite u krug pribor kojega možeš vidjeti kod zubara. Tko više ne zna, pije 3 put. __, ti počni.");
                list3.add("2.\tGovorite u krug, svaki ima 2 sekunde da veli riječ koja je povezana s riječju koju je rekao korisnik prije. ___, ti počni. Onaj koji više ne zna ili pogriješi, prije 3 put.");
                list3.add("3.\tGovorite u krug, „Na štriku se suši šareni šosić“, onaj koji pogriješi, pije 3 put. ___, ti počni.");
                list3.add("4.\tGovorite u krug, „Na vrh brda vrba mrda“, onaj koji pogriješi pije 2 put. ___, ti počni.");
                list3.add("5.\tGovorite u krug pribor kod automehaničara. Tko više ne zna, pije 3 put. __, ti počni.");
                list3.add("6.\tGovorite u krug korisne predmete iz srednje škole koju je svaki korisnik pohađao. __, ti počni. Tko prvi ostane bez ideje, pije 2 put.");
                list3.add("7.\tGovorite u krug Marvel junake iz filmova. Tko prvi pogriješi ili ne zna, pije 3 put.");
                list3.add("8.\tGovorite X-men junake. Tko prvi pogiješi ili ne zna, pije 2 put.");
                list3.add("9.\tGovorite u kruh programske jezike. Onaj tko pogriješi ili ne zna, pije 4 put.");
                list3.add("10.\tGovorite u krug čudovišta iz bajki. Tko prvi pogriješi ili ne zna, pije 2 put.");
                list3.add("11.\t„Za ili protiv gela za kosu“? Svi glasaju istovremeno, ekipa koja izgubi pije 2 put.");
                list3.add("12.\t„Za ili protiv parfema na curama“? Svi glasaju istovremeno, ekipa koja izgubi pije 2 put.");
                list3.add("13.\t„Za ili protiv seksa u javnosti“? Svi glasaju istovremeno, ekipa koja izgubi pije 2 put.");
                list3.add("14.\t„Za ili protiv tajci na dečkima“? Svi glasaju istovremeno, ekipa koja izgubi pije 2 put.");
                list3.add("15.\t„Za ili protiv komunizma“? Svi glasaju istovremeno, ekipa koja izgubi pije 2 put.");
                list3.add("16.\tGovorite u krug stvari s rupama. Prvi koji pogriješi ili ne zna, pije 3 put.");
                list3.add("17.\t„Rum ili vodka“? Svi glasaju istovremeno, ekipa koja izgubi pije 2 put.");
                list3.add("18.\t„Pivo ili vino“? Svi glasaju istovremeno, ekipa koja izgubi pije 2 put.");
                list3.add("19.\t„Bijelo ili crno vino“? Svi glasaju istovremeno, ekipa koja izgubi pije 2 put.");
                list3.add("20.\t„Marvel ili DC“? Svi glasaju istovremeno, ekipa koja izgubi pije 2 put.");
                list3.add("21.\t„Ići u raj ili biti reinkarniran u bolji život“? Svi glasaju istovremeno, ekipa koja izgubi pije 2 put.");
                list3.add("22.\tSvaki igrač mora reći stvar koju ___ ima više od ___. __, počni. Tko ne zna ili pogriješi, pije 2 put.");
                list3.add("23.\tSvaki igrač mora reći stvar koju ___ ima manje od ___. __, počni. Tko ne zna ili pogriješi, pije 3 put.");
                list3.add("24.\tSvaki igrač mora reći stvar koja mu smeta kod ____. __, ti počni. Tko ne zna ili pogriješi, pije 2 put.");
                list3.add("25.\t„Kosa do koljena ili irokeza“? Svi glasaju istovremeno, ekipa koja izgubi pije 2 put.");
                list3.add("26.\tGovorite države koje okružuju Hrvatsku. Tko pogriješi ili ne zna pije 2 put. Na kome stane, pije 3 put.");
                list3.add("27.\tGovorite države koje okružuju Sloveniju. Tko pogriješi ili ne zna pije 2 put. Na kome stane, pije 3 put.");
                list3.add("28.\tGovorite države koje okružuju Ujedinjeno Kraljevstvo. Tko pogriješi ili ne zna pije 2 put. Na kome stane, pije 3 put.");
                list3.add("29.\t„Države u europskoj uniji“! __, ti počni. Tko pogriješi ili ne zna, pije 3 put. Na kome stane, pije 2 put.");
                list3.add("30.\t___, reci ime, prezime i datum rođenja od svakog igrača. Za koju osobu pogriješiš, ta osoba daje 2 guca.");
                list3.add("31.\tVrijeme je za „Meduzu“! Svaki igrač ima glavu naslonjenu na rub stola. Na kraj odbrojavanja, svatko pogleda u oči jedne osobe. Osobe koje se gledaju u oči, eksaju svoje piće.");
                list3.add("32.\tVrijeme je za jednu belu! Ako imate karte za belu kod sebe, neka svaki igrač uzme jednu. Igrač s najvećom kartom daje 4 guca, igrač s najmanjom pije 2 guca. Ako nemate karte, svaki igrač pije 5 put.");
                list3.add("33.\tVrijeme je za jednu belu! Ako imate karte za belu kod sebe, neka svaki igrač uzme jednu. Igrač s najvećom kartom pije 4 guca, igrač s najmanjom daje 2 guca. Ako nemate karte, svaki igrač pije 3 put.");
                list3.add("34.\tGovorite u krug zadnje filmove koje ste pogledali. Ako je netko još pogledao isto film, oboje piju 3 put.");
                list3.add("35.\tGovorite u krug zadnju seriju koju ste gledali. Ako je netko još pogledao/la istu seriju, oboje piju 3 put.");
                list3.add("36.\tGovorite strategijske video igrice. Tko pogriješi ili ne zna pije 2 put. Na kome stane, pije 3 put.");
                list3.add("37.\tGovorite igrice za PS2. Tko pogriješi ili ne zna pije 2 put. Na kome stane, pije 3 put.");
                list3.add("38.\t„Psi ili mačke“? Svi glasaju istovremeno, ekipa koja izgubi pije 2 put.");
                list3.add("39.\tVrijeme je za „Završi rečenicu“! Svaki igrač veli jednu riječ i idući igrač mora ponoviti sve riječi od prije i dodati svoju. Ne zaboravite na smislenost rečenice! Tko pogriješi, prije 2 put.");
                list3.add("40.\tVrijeme je za „Slap“! Jedan igrač počinje piti svoje piće, zatim igrač poslije njega i tako u krug. Kada prvi igrač prestane piti, isto tako moraju i svi ostali igrači u krug jedan za drugim. Igrač koji prerano počne ili prestane, pije još 3 guca. Ako je slap prošao uspješno, prvi pije 3 guca. __, počni.");
                list3.add("41.\tVrijeme je za „Kategoriju“! __, zmisli neku kategoriju i svi ostali igrači moraju nabrojavati tako dugo dok neko ne pogriješi ili ne zna. Taj pije 3 guca.");
                list3.add("42.\tVrijeme je za higijenu! Svi idite oprati ruke, tko prvi opere, daje 2 guca, tko zadnji, pije 2 guca.");
                list3.add("43.\tGovorite u krug najdraža jela od ___. Tko više ne zna ili pogriješi, pije 3 put. __, ti počni.");
                list3.add("44.\t____, velis „U moje kuferu se nalazi..“ i reci jedna predmet. Svaki igrač mora ponoviti sve predmete igrača prije njega i dodati neki svoj predmet. Tko pogriješi, pije 3 guca.");
                list3.add("45.\t„Frizura iz 80-ih ili odjeća iz 80-ih“? Svi glasaju istovremeno, ekipa koja izgubi pije 2 put.");
                list3.add("46.\tVrijeme je za pjesmu“ __, počni pjevati neku pjesmu i pokaži na igrača koji će nastaviti! To se nastavlja sve dok netko ne pogriješi ili ne zna riječi. Svatko tko je pjevao pije 2 guca.");
                list3.add("47.\t„X-ray pogled ili laser pogled“? Svi glasaju istovremeno, ekipa koja izgubi pije 2 put.");
                list3.add("48.\tGovorite u krug poznate kriminalce. Tko prvi pogriješi ili ne zna, pije 2 put.");
                list3.add("49.\tOdlučite tko je najpametnija osoba u sobi. Ta osoba mora piti 3 put.");
                list3.add("50.\tOdlučite tko je najljepša osoba u sobi. Ta osoba mora piti 3 put.");
                list3.add("51.\tVrijeme je za jedan „BUM“! Brojite od 1, na svaki broj koji sadrži 3 ili je djeljiv s 3 morate reći „bum“. Tko pogriješi, pije 3 guca.");



            Collections.shuffle(list3, new Random(System.nanoTime()));
                TextView words = (TextView) findViewById(R.id.words);
                words.setText(list3.get(index++));

                TextView nasl = (TextView) findViewById(R.id.naziv);
                nasl.setText("IGRA");

                ConstraintLayout lay = (ConstraintLayout) findViewById(R.id.layo);
                lay.setBackgroundResource(R.drawable.nova_pozadina2);

        }

    }


}
