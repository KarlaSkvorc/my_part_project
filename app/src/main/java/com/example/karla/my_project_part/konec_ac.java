package com.example.karla.my_project_part;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.TextView;

public class konec_ac extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);//ZA BAR GORE
        setContentView(R.layout.activity_konec_ac);

        if(jezik.language==1){
            TextView words = (TextView) findViewById(R.id.words);
            words.setText("KRAJ!");
        }
        if(jezik.language==2){
            TextView words = (TextView) findViewById(R.id.words);
            words.setText("KONEC");
        }
        if(jezik.language==3){
            TextView words = (TextView) findViewById(R.id.words);
            words.setText("END!");
        }
    }
}
