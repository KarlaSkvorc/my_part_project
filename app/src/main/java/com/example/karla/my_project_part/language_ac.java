package com.example.karla.my_project_part;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

public class language_ac extends AppCompatActivity {
    private Button buttonHr;
    private Button buttonSl;
    private Button buttonAn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);//ZA BAR GORE
        setContentView(R.layout.activity_language_ac);

        buttonHr = (Button) findViewById(R.id.buttonHrv);
        buttonHr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                jezik.language=1;
                openHrvatski();
            }
        });

        buttonSl = (Button) findViewById(R.id.buttonSlo);
        buttonSl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                jezik.language=2;
                openSlovenski();
            }
        });

        buttonAn = (Button) findViewById(R.id.buttonEng);
        buttonAn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                jezik.language=3;
                openEngleski();
            }
        });
    }

    public void openHrvatski(){
        Intent intent1 = new Intent(this, MainActivity.class);
        startActivity(intent1);
    }

    public void openSlovenski(){
        Intent intent2 = new Intent(this, Main2Activity.class);
        startActivity(intent2);
    }

    public void openEngleski(){
        Intent intent3 = new Intent(this, Main3Activity.class);
        startActivity(intent3);
    }
}
